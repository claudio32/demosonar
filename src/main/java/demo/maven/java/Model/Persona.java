package demo.maven.java.Model;

public class Persona {
	
	
	String nombre = "Claudio";
	Integer edad = 28;
	Double altura = 1.83;
	String domicilio = "Paicavi Sur 960 depto 302 Concepción";
	String Palabra = "Hola";
	
	
	public void caminar() {
		System.out.println("Claudio está caminando");
	}
	
	public void comer() { 
		System.out.println("Claudio está comiendo");
	}
	

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getEdad() {
		return edad;
	}

	public void setEdad(Integer edad) {
		this.edad = edad;
	}

	public Double getAltura() {
		return altura;
	}

	public void setAltura(Double altura) {
		this.altura = altura;
	}

	public String getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}
	

	public Persona(String nombre, Integer edad, Double altura, String domicilio) {
		super();
		this.nombre = nombre;
		this.edad = edad;
		this.altura = altura;
		this.domicilio = domicilio;
	}
	

	public Persona() {
		super();
	}
	
	
}
 